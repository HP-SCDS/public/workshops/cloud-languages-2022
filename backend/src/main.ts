import express from 'express';
import { addParticipant, deleteParticipant, getParticipant, getParticipants } from './controllers/participantController';
import cors from 'cors';

const app = express();
const router = express.Router();
const PORT = process.env.PORT ?? 4000;

// this allows everything
app.use(cors());

router.use(express.json());
router.get('/participants', getParticipants);
router.get('/participants/:number', getParticipant);
router.post('/participants', addParticipant);
router.delete('/participants/:number', deleteParticipant);

app.use('/', router);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});