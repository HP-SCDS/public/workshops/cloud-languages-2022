import { Participant } from "../types/participant";
import { Response, Request } from "express";
import { ParticipantRepository } from "../repositories/participantRepository";

export const getParticipants = (req: Request<any>, res: Response<Participant[]>): void => {
    res.json(ParticipantRepository.getParticipants());
};

export const getParticipant = (req: Request<any>, res: Response<Participant>): void => {
    const participantNumber = Number(req.params.number);
    if (!participantNumber) {
        res.sendStatus(400); // bad request
        return;
    }

    const participant = ParticipantRepository.getParticipantByNumber(participantNumber);
    if (participant) {
        res.json(participant);
    } else {
        res.sendStatus(404);
    }
};

export const addParticipant = (req: Request<any>, res: Response<Participant>): void => {
    const participant = req.body as Participant;
    if (!participant || !participant.name || !participant.age) {
        res.sendStatus(400); // bad request
        return;
    }

    res.json(ParticipantRepository.addParticipant(participant));
};

export const deleteParticipant = (req: Request<any>, res: Response<any>): void => {
    const participantNumber = Number(req.params.number);
    if (!participantNumber) {
        res.sendStatus(400); // bad request
        return;
    }

    ParticipantRepository.deleteParticipantByNumber(participantNumber);
    res.sendStatus(204);
};