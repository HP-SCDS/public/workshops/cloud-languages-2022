import { Participant } from "../types/participant";

export class ParticipantRepository {
    private static number: number = 0;
    private static participants: Participant[] = [];

    public static getParticipants(): Participant[] {
        return this.participants;
    }

    public static getParticipantByNumber(number: number): Participant | null {
        const participant = this.participants.find(participant => participant.number === number);
        return participant ?? null;
    }

    public static addParticipant(participant: Participant): Participant {
        const newParticipant = {
            name: participant.name,
            age: participant.age,
            number: ++this.number
        };

        this.participants.push(newParticipant);
        return newParticipant;
    }

    public static deleteParticipantByNumber(number: number): void {
        this.participants = this.participants.filter(participant => participant.number !== number);
    }
}