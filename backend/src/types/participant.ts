export interface Participant {
    number?: number,
    name: string,
    age: number
}