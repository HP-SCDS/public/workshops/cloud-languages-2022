# Lenguajes del Cloud 2022

## Backend

Dentro del directorio `backend` está la versión terminada del backend. Para ejecutarlo primero hay que instalar los paquetes necesarios:

```
cd backend
npm install
```

Luego ya se puede arrancar:

```
npm start
```

Ejemplos con CURL (aunque se puede usar Postman o lo que apetezca) de POST y DELETE, insertar un participante:

```
curl -d '{"name": "Guillermo", "age": 38}' -H "Content-Type: application/json" -X POST http://localhost:4000/participants
```

Borrar un participante:

```
curl -X DELETE http://localhost:4000/participants/1
```

## Frontend
Dentro del directorio `squid-game` está la versión terminada del frontend en React. Para ejecutarlo, previamente hay que instalar las dependencias del proyecto una vez clonado:


```
cd backend
npm install
```

Una vez finalizado el proceso, podremos arrancar el frontend. Ten en cuenta que necesitarás tener el backend arrancado para que funcione.

```
npm start
```

Eso es todo, ¡a jugar!
