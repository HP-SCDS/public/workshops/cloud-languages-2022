import React, { useEffect, useState } from "react";
import { Button, Container, Row } from "react-bootstrap";
import { ActiveGamer, GamerStatus, RegisteredGamer } from "../../models/gamer";
import "./board.scss";


type BoardProps = {
    gamers: RegisteredGamer[]
}

const BoardComponent: React.FC<BoardProps> = ({gamers}) => {
    const [activeGamers, setActiveGamers] = useState<ActiveGamer[]>([]);
    const [winner, setWinner] = useState<ActiveGamer>();
    const [round, setRound] = useState(0);

    const initActiveGamers = () => {
        const activeGamers = gamers.map((gamer: RegisteredGamer) => {
            return {
                age: gamer.age,
                name: gamer.name,
                number: gamer.number,
                status: GamerStatus.stopped,
                round: 0,
            } as ActiveGamer
        });

        setActiveGamers(activeGamers);
    }

    const killRandomGamers = () => {
        if (!activeGamers) {
            return;
        }

        const totalActiveGamers = activeGamers.length;
        if (totalActiveGamers === 1) {
            return;
        }

        const gamersToKill = Math.floor(Math.random() * totalActiveGamers);
        for (let killedGamersIndex = 0; killedGamersIndex < gamersToKill; killedGamersIndex++) {
            const randomIndex = Math.floor(Math.random() * totalActiveGamers);
            activeGamers[randomIndex].status = GamerStatus.moving;
        }

        const aliveGamers = activeGamers.filter((gamer: ActiveGamer) => {
            return gamer.status === GamerStatus.stopped;
        });

        setActiveGamers(aliveGamers);
        const nextRound = round + 1;
        setRound(nextRound);

        if (aliveGamers.length === 1) {
            setWinner(aliveGamers[0]);
        }
    }

    const handleRound = () => {
        killRandomGamers();
    }

    const handleReset = () => {
        initActiveGamers();
        setWinner(undefined);
        setRound(0);
    }

    useEffect(() => {
        initActiveGamers();
    }, []);

    return (
        <Container className="game-container">
            {activeGamers.length > 1 ? 
                <>
                    <Row>Round {round}!</Row>
                    {activeGamers.map((registeredGamer: RegisteredGamer) => {
                        return (
                            <Row key={registeredGamer.number}>
                                {registeredGamer.name} - {registeredGamer.age}
                            </Row>
                        )
                    })}
                    <Row>
                        {round === 0 ? 
                            <Button variant="primary" onClick={handleRound}>Start!</Button> : 
                            <Button variant="primary" onClick={handleRound}>Next Round!</Button>
                        }
                    </Row>
                </> : 
                <div>
                    <div>
                        <span>You win, {winner?.name}!</span>
                    </div>
                    <Button variant="primary" onClick={handleReset}>Reset!</Button>
                </div>
            }
        </Container>
    );
}

export default BoardComponent;