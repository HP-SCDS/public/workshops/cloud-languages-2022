import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Button, Card, Col, Row } from "react-bootstrap";
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";
import { RegisteredGamer } from "../../models/gamer";

type GamersProps = {
    gamers: RegisteredGamer[];
    handleRemove: (id: number) => Promise<void>;
}

const Gamers: React.FC<GamersProps> = ({gamers, handleRemove}) => {
    return (
        <Row>{
            gamers.map((gamer: RegisteredGamer) => {
                return (
                    <Col key={gamer.number}>
                        <Card className="board-card">
                            <Card.Body>
                                <Card.Title>{gamer.name}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">Age: {gamer.age}</Card.Subtitle>
                                <Button variant="danger" className="justify-content-end" onClick={() => handleRemove(gamer.number)}><FontAwesomeIcon icon={faTrashCan} /></Button>
                            </Card.Body>
                        </Card>
                    </Col>
                );
            })}
        </Row>
    );
}

export default Gamers;