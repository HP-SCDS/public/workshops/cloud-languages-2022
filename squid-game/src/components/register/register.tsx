import React, { useCallback, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { Gamer } from "../../models/gamer";
import { registerGamer } from "../../services/gamers";
import "./register.scss";

type LoginProps = {};

const Register: React.FC<LoginProps> = () => {
    const [rejected, setRejected] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const {
        register, 
        handleSubmit, 
        formState: { errors }
    } = useForm<Gamer>();
    const navigate = useNavigate();
    
    const onSubmit = useCallback(async (gamer: Gamer) => {
        setSubmitted(true);
        setRejected(false);
        try {
            const registeredGamer = await registerGamer(gamer);
            navigate("/board", {state: {registeredGamer}});
        }catch(error) {
            setRejected(true);
        }
    }, [navigate]);

    return (
        <Card>
            <Card.Body>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-group formContainer">
                        <label>Name</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="formGamerName" 
                            placeholder="Name" 
                            {...register("name", {
                                required: true,
                                minLength: 3,
                                maxLength: 20
                            })} 
                        />
                    </div>
                    <div>
                        {errors?.name?.type === "required" && <p>This field is required</p>}
                        {errors?.name?.type === "maxLength" && (
                            <p>Max length 20</p>
                        )}
                        {errors?.name?.type === "minLength" && (
                            <p>Min length 3</p>
                        )}
                    </div>
                    <div className="form-group formContainer">
                        <label>Age</label>
                        <input 
                            type="number" 
                            className="form-control" 
                            id="formGamerAge" 
                            placeholder="Age" 
                            {...register("age", {
                                required: true,
                                valueAsNumber: true,
                                min: 1,
                            })} 
                        />
                    </div>
                    <div>
                        {errors?.age?.type === "required" && <p>This field is required</p>}
                        {errors?.age?.type === "min" && (
                            <p>Min age 1</p>
                        )}
                    </div>
                    {submitted && rejected && <p>Gamer cannot be registered</p>}
                    
                    <div className="formSubmit">
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </div>
                </form>
            </Card.Body>
        </Card>
    );
}

export default Register;