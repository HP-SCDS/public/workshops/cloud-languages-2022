export enum GamerStatus {
    stopped = "stopped",
    moving = "moviong"
}

export interface Gamer {
    name: string;
    age: number;
}

export interface RegisteredGamer extends Gamer {
    number: number;
}

export interface ActiveGamer extends RegisteredGamer {
    status: GamerStatus;
}