import { faSkullCrossbones } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Alert, Button, Col, Container, Modal, Row } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";
import BoardComponent from "../../components/board/board";
import Gamers from "../../components/gamers/gamers";
import { RegisteredGamer } from "../../models/gamer";
import { removeGamer, getAllGamers, getGamer } from "../../services/gamers";
import "./board.scss";

type BoardProps = {};
type LocationState = {
    registeredGamer: RegisteredGamer;
};

const Board: React.FC<BoardProps> = () => {
    const location = useLocation().state as LocationState;
    const navigate = useNavigate();  
    const [showAlert, setShowAlert] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const handleClose = () => setShowModal(false);
    const [registeredGamer, setRegisteredGamer] = useState<RegisteredGamer>();
    const [gamers, setGamers] = useState<RegisteredGamer[]>([]);
    const [removedGamer, setRemovedGamer] = useState<RegisteredGamer>();
    const [playing, setPlaying] = useState(false);

    const getGamersFromServer = async () => {
        try {
            const gamers = await getAllGamers();
            setGamers(gamers);
        } catch(error) {
            console.log("there was an error getting gamers", {error});
        }
    }

    const getRegisteredGamer = async () => {
        try {
            const gamer = await getGamer(location.registeredGamer.number);
            setRegisteredGamer(gamer);
            setShowAlert(true);
        } catch(error) {
            console.log("there was an error getting gamer", {error});
        }
    }

    const handleRemove = async (id: number) => {
        try {
            const gamer = await getGamer(id);
            setRemovedGamer(gamer);
            await removeGamer(id);
            await getGamersFromServer();
            setShowModal(true);
        }catch (error) {
            console.log("error removing gamer", {error});
        }
    }

    const handlePlay = () => {
        setPlaying(true);
    }

    const handleRegister = () => {
        navigate("/");
    }

    useEffect(() => {
        getRegisteredGamer();
        getGamersFromServer();
    }, []);

    return (
        <Container className="board-container">
            {showAlert ? <Alert variant="primary" onClose={() => setShowAlert(false)} dismissible>
                <Alert.Heading>Welcome!</Alert.Heading>
                <span> Welcome to the game for your life, {registeredGamer?.name}!</span>
            </Alert>
            : null}

            <Modal 
                show={showModal}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}>
                <Modal.Header>
                    <Modal.Title>Gamer Removed!</Modal.Title>
                </Modal.Header>
                <Modal.Body>Gamer {removedGamer?.name} has been removed</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
            
            {playing ? 
                <BoardComponent gamers={gamers}/> : 
                <>
                    <Gamers gamers={gamers} handleRemove={handleRemove}></Gamers>
                    <Row>
                        <Col><Button variant="primary" onClick={handlePlay}><FontAwesomeIcon icon={faSkullCrossbones} /> Play!</Button></Col>
                        <Col><Button variant="outline-primary" onClick={handleRegister}>Register</Button></Col>
                    </Row>
                </>
            }
        </Container>
    );
}

export default Board;