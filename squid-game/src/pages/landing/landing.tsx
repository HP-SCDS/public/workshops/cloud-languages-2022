import React from "react";

import {ReactComponent as SquidLogo} from "../../assets/squid-game.svg";
import Register from "../../components/register/register";
import "./landing.scss";

type LandingProps = {};

const Landing: React.FC<LandingProps> = () => {
    return (<div className="container">
        <SquidLogo className="logo" />
        <Register />
    </div>);
}

export default Landing;