import { Gamer, RegisteredGamer } from "../models/gamer";

const serverUrl: string = "http://localhost:4000";
const gamerPath: string = "/participants"

export const registerGamer = async (gamer: Gamer): Promise<RegisteredGamer> => {
    const url = `${serverUrl}${gamerPath}`;
    const response = await fetch(url, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(gamer)
    });

    if (response.status !== 200) {
        throw new Error(`there was an error creating gamer: ${response.status}`)
    }

    return response.json();
}

export const getAllGamers = async (): Promise<RegisteredGamer[]> => {
    const url = `${serverUrl}${gamerPath}`;
    const response = await fetch(url, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`there was an error getting gamers: ${response.status}`)
    }

    return response.json();
}

export const getGamer = async (id: number): Promise<RegisteredGamer> => {
    const url = `${serverUrl}${gamerPath}/${id}`;
    const response = await fetch(url, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`there was an error getting gamer: ${response.status}`)
    }

    return response.json();
}

export const removeGamer = async (id: number) => {
    const url = `${serverUrl}${gamerPath}/${id}`;
    const response = await fetch(url, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 204) {
        throw new Error(`there was an error deleting gamer: ${response.status}`);
    }
}